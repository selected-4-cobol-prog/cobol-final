       IDENTIFICATION DIVISION.
       PROGRAM-ID. TRADER.
       AUTHOR. NITHIPHAT.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT  TRADER-FTLE ASSIGN TO  "trader1.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT  TRADER-REPORT-FTLE ASSIGN TO "trader.rpt"
              ORGANIZATION IS SEQUENTIAL.
       DATA DIVISION.
       FILE SECTION.
       FD  TRADER-FTLE.
       01 TRADER-DETAIL.
            88 END-OF-TRADER-FTLE VALUE  HIGH-VALUES.
            05 MEMBER-ID      PIC 9999.
            05 PROVINCE-NUM   PIC 99.
            05 TRADER-SUM     PIC 9(7)V99.

       WORKING-STORAGE SECTION.
       01 PROVINCE-TABLE.
           05 PROVINCE-TOTALS    OCCURS 77 TIMES.
              10 PROVINCE-TOTAL PIC 9(8)V99.
              10 PROVINCE-COUNT PIC 9(5).
       01 PROVINCE-IDX          PIC 9(2).
       01 REPORT-HEDING1        PIC X(47)
           VALUE "PPROVINCE   P INCOME   MEMBER  MEMBER INCOME".
       01 DETAIL-LINE.
           05 PRN-PROVINCE-NUM  PIC BZ9.
           05 PRN-P-INCOME      PIC B(5)$$$,$$$,$$9.99.
           05 PRN-MEMBER        PIC B(3)ZZ,ZZ9.
           05 PRN-MEMBER-INCOME PIC BB$$$,$$$,$$9.99.
       01 US-TOTALS.
           05 US-TOTAL            PIC 9(9)v99.
           05 US-MEMBER-COUNT     PIC 9(6).
           05 PRN-US-TOTAL        PIC $,$$$,$$$,$$9.99.
           05 PRN-US-MEMBER       PIC B(9)ZZZ,ZZ9.
           05 PRN-US-MEMBER-INCOME PIC BBBB$$$,$$$,$$9.99. 
       
       PROCEDURE DIVISION.
       BEGIN.
           MOVE ZERO TO PROVINCE-TABLE
           OPEN INPUT TRADER-FTLE
           READ TRADER-FTLE
              AT END SET END-OF-TRADER-FTLE TO TRUE END-READ
           PERFORM UNTIL END-OF-TRADER-FTLE
               ADD TRADER-SUM  TO PROVINCE-TOTAL US-TOTAL    
               ADD 1 TO PROVINCE-COUNT , US-MEMBER-COUNT 
               READ TRADER-FTLE 
                 AT END SET END-OF-TRADER-FTLE TO TRUE
               END-READ
           END-PERFORM
           PERFORM PRINT-RESULTS

           CLOSE TRADER-FTLE
           STOP RUN.

       PRINT-RESULTS.
           DISPLAY  REPORT-HEDING1 
           PERFORM VARYING PROVINCE-IDX   FROM  1 BY 1
                   UNTIL PROVINCE-IDX  GREATER THAN 77
              MOVE PROVINCE-IDX   TO PRN-PROVINCE-NUM
              MOVE PROVINCE-TOTAL (PROVINCE-IDX) TO PRN-PROVINCE-IDX 
              MOVE PROVINCE-COUNT (PROVINCE-IDX) TO PRN-PROVINCE-COUNT
              COMPUTE  PRN-MEMBER-INCOME = PROVINCE-TOTAL / 
                    PROVINCE-COUNT
              DISPLAY DETAIL-LINE 
           END-PERFORM
           MOVE  US-TOTAL  TO PRN-US-TOTAL   
           MOVE  US-MEMBER-COUNT  TO PRN-US-MEMBER-INCOME
           COMPUTE  PRN-US-MEMBER-INCOME = US-TOTAL / US-MEMBER-COUNT
           DISPLAY  "SUM"  PRN-US-MEMBER-INCOME.

            


              
           